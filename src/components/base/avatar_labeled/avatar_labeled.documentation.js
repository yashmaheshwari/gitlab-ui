import description from './avatar_labeled.md';

export default {
  followsDesignSystem: true,
  description,
};
